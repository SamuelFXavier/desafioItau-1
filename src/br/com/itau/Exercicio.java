package br.com.itau;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import br.com.itau.modelo.Lancamento;
import br.com.itau.service.LancamentoService;

public class Exercicio {

	public static void main(String[] args) {
		List<Lancamento> lancamentos = new LancamentoService().listarLancamentos();

		//TODO: continue daqui
		System.out.println("Hello");

		System.out.println("Precisamos escrever no console os gastos ordenados por meses.");
		lancamentos.stream().sorted(Comparator.comparing(lancamento -> lancamento.getMes()))
				.forEach(lancamento -> System.out.printf(" Gastos do mes %d, valor: %.2f%n ", lancamento.getMes(),lancamento.getValor() ));

		System.out.println();
		System.out.println("Escrever todos os lancamentos de uma mesma categoria de sua escolha.");
		List<Lancamento> filtrarPorCategoria = lancamentos.stream().filter(lancamento -> lancamento.getCategoria() == 6 ).collect(Collectors.toList());
		filtrarPorCategoria.forEach(lancamento -> System.out.println(lancamento ));

		System.out.println();
		System.out.println("Mostrar o total da fatura de um mes em específico de sua escolha.");
		Double totalFaturaMes = lancamentos.stream().filter(lancamento -> lancamento.getMes() == 7).collect(Collectors.summingDouble(l -> l.getValor()));
		System.out.printf("Mes 7, valor Total fatura " +  totalFaturaMes);
	}

}

